using System;
using Xunit;
namespace Veeam.Collections.Tests
{
    public class PriorityQueueTest
    {
        [Fact]
        public void Init_ShouldBe_Empty()
        {
            // Arrange
            var queue = new PriorityQueue<Int64, String>();

            // Act
            var isEmpty = queue.IsEmpty();

            // Assert
            Assert.True(isEmpty);
        }

        [Fact]
        public void Enqueue_ShouldBe_NotEmpty()
        {
            // Arrange
            var queue = new PriorityQueue<Int64, String>();

            // Act
            queue.Enqueue("Some item", 0);
            var isEmpty = queue.IsEmpty();

            // Assert
            Assert.False(isEmpty);
        }

        [Fact]
        public void Enqueue_UnorderedItems_Should_DeuqueueOrdered()
        {
            // Arrange
            var queue = new PriorityQueue<Int64, String>();

            // Act
            queue.Enqueue("Second item", 2);
            queue.Enqueue("First item", 1);
            queue.Enqueue("Third item", 3);
            var firstActualItem = queue.Dequeue();
            var secondActualItem = queue.Dequeue();
            var thirdActualItem = queue.Dequeue();

            // Assert
            Assert.Equal("First item", firstActualItem);
            Assert.Equal("Second item", secondActualItem);
            Assert.Equal("Third item", thirdActualItem);
        }

        [Fact]
        public void Dequeue_WhenEmpty_Should_ThrowException()
        {
            // Arrange
            var queue = new PriorityQueue<Int64, String>();

            // Act
            var exception = Assert.Throws<InvalidOperationException>(() => queue.Dequeue());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal("The queue is empty.", exception.Message);
        }
    }
}
