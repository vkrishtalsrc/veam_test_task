﻿using System;
using System.Threading.Tasks;
using Xunit;

namespace Veeam.Collections.Tests
{
    public sealed class SynchronizedPriorityQueueTest
    {
        [Fact]
        public void Init_ShouldBe_Empty()
        {
            // Arrange
            var queue = new SynchronizedPriorityQueue<Int32, String>();

            // Act
            var isEmpty = queue.IsEmpty();

            // Assert
            Assert.True(isEmpty);
        }

        [Fact]
        public void Enqueue_ShouldBe_NotEmpty()
        {
            // Arrange
            var queue = new SynchronizedPriorityQueue<Int32, String>();

            // Act
            queue.Enqueue("Some item", 0);
            var isEmpty = queue.IsEmpty();

            // Assert
            Assert.False(isEmpty);
        }

        [Fact]
        public void InitWithSpecifiedMaxSize_ShouldBe_Empty()
        {
            // Arrange
            var queue = new SynchronizedPriorityQueue<Int32, String>(10);

            // Act
            var isEmpty = queue.IsEmpty();

            // Assert
            Assert.True(isEmpty);
        }

        [Fact]
        public void Enqueue_UnorderedItems_Should_DeuqueueOrdered()
        {
            // Arrange
            var queue = new SynchronizedPriorityQueue<Int32, String>();

            // Act
            queue.Enqueue("Second item", 2);
            queue.Enqueue("First item", 1);
            queue.Enqueue("Third item", 3);
            queue.Dequeue(out var firstActualItem);
            queue.Dequeue(out var secondActualItem);
            queue.Dequeue(out var thirdActualItem);

            // Assert
            Assert.Equal("First item", firstActualItem);
            Assert.Equal("Second item", secondActualItem);
            Assert.Equal("Third item", thirdActualItem);
        }

        [Fact]
        public void ParallelEnqueueAndDequeue_ShouldBe_Success()
        {
            var items = 1000;
            var queue = new SynchronizedPriorityQueue<Int32, Int32>();

            // consumer dequeues items until it gets as many as it expects
            var consumer = Task.Run(() =>
            {
                var item = 0;
                var lastReceived = 0;
                while (true)
                {
                    Assert.True(queue.Dequeue(out item));
                    Assert.Equal(lastReceived + 1, item);
                    lastReceived = item;
                    if (item == items)
                    {
                        break;
                    }
                }

                Assert.Equal(items, lastReceived);
            });

            // producer queues the expected number of items
            var producer = Task.Run(() =>
            {
                for (var i = 1; i <= items; i++)
                {
                    queue.Enqueue(i, i);
                }
            });

            Task.WaitAll(producer, consumer);
        }

        [Fact]
        public void ParallelEnqueueAfterDequeue_ShouldBe_Success()
        {
            var queue = new SynchronizedPriorityQueue<Int32, Int32>();
            var dequeue = Task.Run(() =>
            {
                Assert.True(queue.IsEmpty());
                Assert.True(queue.Dequeue(out var item));
                Assert.Equal(1, item);
            });

            var enqueue = Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(3));
                queue.Enqueue(1, 0);
            });

            Task.WaitAll(dequeue, enqueue);
        }

        [Fact]
        public void ParallelEnqueueAndDequeue_WithSpecifiedMaxSize_ShouldBe_Success()
        {
            var items = 1000;
            var queue = new SynchronizedPriorityQueue<Int32, Int32>(16);

            // consumer dequeues items until it gets as many as it expects
            var consumer = Task.Run(() =>
            {
                var item = 0;
                var lastReceived = 0;
                while (true)
                {
                     Assert.True(queue.Dequeue(out item));
                    Assert.Equal(lastReceived + 1, item);
                    lastReceived = item;
                    if (item == items)
                    {
                        break;
                    }
                }

                Assert.Equal(items, lastReceived);
            });

            // producer queues the expected number of items
            var producer = Task.Run(() =>
            {
                for (var i = 1; i <= items; i++)
                {
                    queue.Enqueue(i, i);
                }
            });

            Task.WaitAll(producer, consumer);
        }
    }
}
