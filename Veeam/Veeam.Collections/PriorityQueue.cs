﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Veeam.Collections
{
    /// <summary>
    /// Represents a not thread-safety priority queue.
    /// </summary>
    /// <typeparam name="TPriority">The type of priority.</typeparam>
    /// <typeparam name="TItem">The type of items to enqueue.</typeparam>
    public sealed class PriorityQueue<TPriority, TItem>
        where TPriority : IComparable
    {
        /// <summary>
        /// The internal sorted dictionary. Used like a priority heap.
        /// </summary>
        private readonly SortedDictionary<TPriority, TItem> m_sortedDictionary = new SortedDictionary<TPriority, TItem>();

        /// <summary>
        /// Adds the item to the queue.
        /// </summary>
        /// <param name="item">The item to enqueue.</param>
        /// <param name="priority">The item priority.</param>
        public void Enqueue(TItem item, TPriority priority)
        {
            m_sortedDictionary.Add(priority, item);
        }

        /// <summary>
        /// Removes and returns
        /// </summary>
        /// <exception cref="InvalidOperationException">The queue is empty.</exception>
        /// <returns></returns>
        public TItem Dequeue()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException($"The queue is empty.");
            }

            var item = m_sortedDictionary.First();
            m_sortedDictionary.Remove(item.Key);
            return item.Value;
        }

        /// <summary>
        /// Returns false if queue does not contain any elements; otherwise, true.
        /// </summary>
        public Boolean IsEmpty()
        {
            return !m_sortedDictionary.Any();
        }

        /// <summary>
        /// Returns the count of elements in the queue.
        /// </summary>
        public Int32 Count()
        {
            return m_sortedDictionary.Count();
        }
    }
}
