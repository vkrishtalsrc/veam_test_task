﻿using System;
using System.Threading;

namespace Veeam.Collections
{
    // Why we need it?
    // We could try to use System.Collections.Concurrent.ConcurrentQueue<T> or BlockingCollection<T> classes instead this custom implementation.
    // However test task contains specific requirements to avoid existing high level mechanisms of thread synchronization.
    // More information: https://docs.microsoft.com/en-us/dotnet/api/system.collections.concurrent.concurrentqueue-1?view=netcore-3.1
    // And: https://docs.microsoft.com/en-us/dotnet/api/system.collections.concurrent.blockingcollection-1?view=netcore-3.1

    /// <summary>
    /// Represents a thread-safety priority queue.
    /// </summary>
    /// <typeparam name="TPriority">The type of item priority.</typeparam>
    /// <typeparam name="TItem">The type of items to enqueue.</typeparam>
    public sealed class SynchronizedPriorityQueue<TPriority, TItem>
        where TPriority : IComparable
    {
        /// <summary>
        /// The guard to lock critical section.
        /// </summary>
        private readonly Object m_lock = new Object();

        /// <summary>
        /// The internal not thread-safety queue.
        /// </summary>
        private readonly PriorityQueue<TPriority, TItem> m_queue = new PriorityQueue<TPriority, TItem>();

        /// <summary>
        /// The max size of queue.
        /// </summary>
        private readonly UInt32 m_maxSize;

        /// <summary>
        ///
        /// </summary>
        private volatile Int32 m_producers;

        /// <summary>
        ///
        /// </summary>
        public Boolean NewItemsExpected => m_producers > 0;

        /// <summary>
        /// Creates a new instance of <see cref="SynchronizedPriorityQueue{TPriority, TItem}"/> class.
        /// </summary>
        /// <param name="maxSize">The max possible size of queue.</param>
        public SynchronizedPriorityQueue(UInt32 maxSize = uint.MaxValue, Int32 producers = 1)
        {
            m_maxSize = maxSize;
            m_producers = producers;
        }

        /// <summary>
        /// Adds the item to the queue.
        /// </summary>
        /// <remarks>
        /// The method can block current thread in case:
        ///     1. Other thread have already locked the queue.
        ///     2. The capacity of the queue is over. Thread will wait other thread removes items.
        /// </remarks>
        /// <param name="item">The item to enqueue.</param>
        /// <param name="priority">The item priority.</param>
        public void Enqueue(TItem item, TPriority priority)
        {
            lock (m_lock)
            {
                while (m_queue.Count() >= m_maxSize)
                {
                    // the queue is full
                    // wait another thread dequeue an item
                    Monitor.Wait(m_lock);
                }

                m_queue.Enqueue(item, priority);

                // signal other threads about new item in queue
                Monitor.PulseAll(m_lock);
            }
        }

        /// <summary>
        /// Removes and returns the item from the beginning of the queue.
        /// </summary>
        /// <remarks>
        /// The method can block current thread in case:
        ///     1. Other thread have already locked the queue.
        ///     2. The queue is empty. The thread will wait other thread adds new items.
        /// </remarks>
        /// <returns>The item from queue.</returns>
        public Boolean Dequeue(out TItem result)
        {
            lock (m_lock)
            {
                while (m_queue.IsEmpty())
                {
                    if (!NewItemsExpected)
                    {
                        // nothing to return
                        result = default;
                        return false;
                    }

                    // the queue is empty but we can expect new items
                    // wait another thread enqueue something
                    Monitor.Wait(m_lock);
                }

                result = m_queue.Dequeue();

                // signal other thread about removed item
                Monitor.PulseAll(m_lock);
                return true;
            }
        }

        /// <summary>
        /// Returns false if queue does not contain any elements; otherwise, true.
        /// </summary>
        public Boolean IsEmpty()
        {
            lock (m_lock)
            {
                return m_queue.IsEmpty();
            }
        }

        public void Release()
        {
            // inspired by idea of BlockingCollection.CompleteAdding() method

            lock (m_lock)
            {
                m_producers--;
                Monitor.PulseAll(m_lock);
            }
        }
    }
}