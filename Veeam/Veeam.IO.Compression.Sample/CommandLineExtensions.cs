﻿using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Diagnostics;

namespace Veeam.IO.Compression.Sample
{
    /// <summary>
    /// Helper extensions methods for <see cref="System.CommandLine"/>.
    /// </summary>
    internal static class CommandLineExtensions
    {
        /// <summary>
        /// Adds command options.
        /// </summary>
        public static Command WithOptions(this Command self, IEnumerable<Option> options)
        {
            Debug.Assert(self != null);
            Debug.Assert(options != null);

            foreach (var option in options)
            {
                self.AddOption(option);
            }

            return self;
        }

        /// <summary>
        /// Sets command handler.
        /// </summary>
        public static Command WithHandler<T1, T2, T3>(this Command self, Action<T1, T2, T3> handler)
        {
            Debug.Assert(self != null);
            Debug.Assert(handler != null);

            self.Handler = CommandHandler.Create(handler);
            return self;
        }

        /// <summary>
        /// Sets command handler.
        /// </summary>
        public static Command WithHandler<T1, T2, T3, T4>(this Command self, Action<T1, T2, T3, T4> handler)
        {
            Debug.Assert(self != null);
            Debug.Assert(handler != null);

            self.Handler = CommandHandler.Create(handler);
            return self;
        }
    }
}