﻿using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Konsole;

namespace Veeam.IO.Compression.Sample
{
    /// <summary>
    /// Represents a usage example of Veeam.IO.Compression library.
    /// The executable file name is GZipTest.exe
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The default size of data block to compress.
        /// Every block compressing independently.
        /// </summary>
        private const Int32 DEFAULT_COMPRESSION_BLOCK_SIZE_IN_BYTES = 1024 * 1024; // 1MB

        /// <summary>
        /// The count of thread to compress or decompress data.
        /// It is more optimal way to use one thread by processor core.
        /// Note: Environment.ProcessorCount is count of logical (not physical) processors.
        /// </summary>
        private static readonly Int32 DEFAULT_COMPRESSION_THREAD_COUNT = Environment.ProcessorCount;

        /// <summary>
        /// The application entry point.
        /// </summary>
        /// <returns>0 if success, otherwise 1</returns>
        public static Int32 Main(String[] args)
        {
            return GetCommandLineInterface().Invoke(args);
        }

        /// <summary>
        /// Returns object which executes commands from command line.
        /// </summary>
        private static RootCommand GetCommandLineInterface()
        {
            // get options
            var common = GetCommonOptions();
            var compressionOnly = GetCompressionOptions();

            // create commands
            return new RootCommand()
            {
                new Command("compress", "Compresses a specified file")
                    .WithOptions(common)
                    .WithOptions(compressionOnly)
                    .WithHandler<String, String, Int32, Int32>(Compress),

                new Command("decompress", "Decompresses a specified file")
                    .WithOptions(common)
                    .WithHandler<String, String, Int32>(Decompress)
            };
        }

        /// <summary>
        /// Returns common options for all command line commands.
        /// </summary>
        private static IEnumerable<Option> GetCommonOptions()
        {
            yield return new Option<String>(
                alias: "--source",
                description: "Source file name")
            {
                Required = true
            };

            yield return new Option<String>(
                alias: "--destination",
                description: "Destination file name")
            {
                Required = true
            };

            yield return new Option<Int32>(
                alias: "--threads",
                getDefaultValue: () => DEFAULT_COMPRESSION_THREAD_COUNT,
                description: "Count of processing threads")
            {
                Required = false
            };
        }

        /// <summary>
        /// Returns options for compression command only.
        /// </summary>
        private static IEnumerable<Option> GetCompressionOptions()
        {
            yield return new Option<Int32>(
                alias: "--block-size",
                getDefaultValue: () => DEFAULT_COMPRESSION_BLOCK_SIZE_IN_BYTES,
                description: "Size of one processing block in bytes (1MB by default)")
            {
                Required = false
            };
        }

        /// <summary>
        /// Compresses the specified file,
        /// </summary>
        /// <param name="source">The source file to compress.</param>
        /// <param name="destination">The destination to save compressed file.</param>
        /// <param name="threads">The count of task threads.</param>
        /// <param name="blockSize">The size of data block to compress.</param>
        private static void Compress(String source, String destination, Int32 threads, Int32 blockSize)
        {
            var taskFactory = new GZipTaskFactory();
            using var task = taskFactory.Compress(source, destination, threads, blockSize);
            Execute(task, new FileInfo(source));
        }

        /// <summary>
        /// Decompresses the specified file.
        /// </summary>
        /// <param name="source">The source compressed file to decompress.</param>
        /// <param name="destination">The destination to save decompressed file.</param>
        /// <param name="threads">The count of task threads.</param>
        private static void Decompress(String source, String destination, Int32 threads)
        {
            var taskFactory = new GZipTaskFactory();
            using var task = taskFactory.Decompress(source, destination, threads);
            Execute(task, new FileInfo(source));
        }

        /// <summary>
        /// Executes a zip task.
        /// </summary>
        /// <param name="task">The task to execute.</param>
        /// <param name="sourceFile">the source file info.</param>
        private static void Execute(IGZipTask task, FileInfo sourceFile)
        {
            // Start task execution in new thread without blocking current.
            task.Start();

            // If you need to block the current thread until task finished:
            // task.Wait();

            // Wait until tasks finished and print progress bar
            TrackProgress(task, sourceFile);
        }

        /// <summary>
        /// Tracks zip task progress and draws progress bar.
        /// </summary>
        /// <param name="task">The zip task track.</param>
        private static void TrackProgress(IGZipTask task, FileInfo sourceFile)
        {
            Debug.Assert(task.IsRunning);

            // create console progress bar
            var progressBar = new ProgressBar(PbStyle.SingleLine, 100, 10);

            // update progress bar
            Int64 prevProgress = 0;
            while (task.IsRunning)
            {
                if (prevProgress != task.Progress)
                {
                    var percents = Convert.ToInt32((Double) task.Progress / sourceFile.Length * 100);
                    progressBar.Refresh(percents, "Processing");
                    prevProgress = task.Progress;
                }
                else
                {
                    // System call, which in turn triggers an interruption that allows the operating system to take control.
                    // The operating system detects the call to sleep and marks current thread as blocked and can activate other thread ASAP.
                    // It can boost compression\decompression threads little bit.
                    Thread.Sleep(1);
                }
            }

            if (task.Result.Success)
            {
                progressBar.Refresh(100, "Completed");
            }
        }
    }
}