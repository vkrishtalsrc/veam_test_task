﻿using System;
using System.IO;
using Xunit;

namespace Veeam.IO.Compression.Tests
{
    public class GZipTaskFactoryTest
    {
        [Fact]
        public void CreateCompressTask_ShouldBe_Success()
        {
            // Arrange
            var factory = new GZipTaskFactory();
            using var source = new MemoryStream();
            using var destination = new MemoryStream();

            // Act
            var task = factory.Compress(source, destination);

            // Assert
            Assert.NotNull(task);
            Assert.False(task.IsRunning);
            Assert.Null(task.Result);
        }

        [Fact]
        public void CreateDecompressTask_ShouldBe_Success()
        {
            // Arrange
            var factory = new GZipTaskFactory();
            using var source = new MemoryStream();
            using var destination = new MemoryStream();

            // Act
            var task = factory.Compress(source, destination);

            // Assert
            Assert.NotNull(task);
            Assert.False(task.IsRunning);
            Assert.Null(task.Result);
        }

        [Fact]
        public void CreateCompressTask_WithInvalidFileName_Should_ThrowException()
        {
            // Arrange
            var factory = new GZipTaskFactory();

            // Act
            var exception = Assert.Throws<FileNotFoundException>(
                () => factory.Compress("invalid_file_name", "invalid_file_name"));

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void CreateDecompressTask_WithInvalidFileName_Should_ThrowException()
        {
            // Arrange
            var factory = new GZipTaskFactory();

            // Act
            var exception = Assert.Throws<FileNotFoundException>(
                () => factory.Decompress("invalid_file_name", "invalid_file_name"));

            // Assert
            Assert.NotNull(exception);
        }
    }
}