﻿using System;
using System.IO;
using System.Linq;
using Xunit;

namespace Veeam.IO.Compression.Tests
{
    public sealed class GZipTaskTest
    {
        [Fact]
        public void Wait_ShouldBe_Success()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Wait();

            Assert.NotNull(compressTask.Result);
            Assert.True(compressTask.Result.Success);
            Assert.Null(compressTask.Result.Exception);
        }

        [Fact]
        public void StartAndWait_ShouldBe_Success()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Start();
            compressTask.Wait();

            Assert.NotNull(compressTask.Result);
            Assert.True(compressTask.Result.Success);
            Assert.Null(compressTask.Result.Exception);
        }

        [Fact]
        public void StartTwice_ShouldBe_Success()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            Enumerable.Repeat(0, 10000).ToList().ForEach(i => writer.Write(i));

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream, true, true);

            // Act
            compressTask.Start();
            compressTask.Start();
        }

        [Fact]
        public void Compress_And_Decompress_ShouldBe_Success()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream, true, true);

            using var decompressedStream = new MemoryStream();
            using var decompressTask = new GZipTaskFactory().Decompress(compressStream, decompressedStream, true, true);


            // Act
            compressTask.Wait();
            decompressTask.Wait();

            // Assert
            Assert.NotNull(compressTask.Result);
            Assert.True(compressTask.Result.Success);
            Assert.Null(compressTask.Result.Exception);
            Assert.NotNull(decompressTask.Result);
            Assert.True(decompressTask.Result.Success);
            Assert.Null(decompressTask.Result.Exception);

            decompressedStream.Seek(0, SeekOrigin.Begin);
            using var reader = new BinaryReader(decompressedStream);
            var actual = reader.ReadString();
            Assert.Equal("Hello world", actual);
        }

        [Fact]
        public void DisposeTwice_ShouldBe_Success()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Dispose();
            compressTask.Dispose();
        }

        [Fact]
        public void Dispose_And_GetResult_Should_ThrowException()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Dispose();
            var exception = Assert.Throws<ObjectDisposedException>(() => compressTask.Result);

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void Dispose_And_GetProgress_Should_ThrowException()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Dispose();
            var exception = Assert.Throws<ObjectDisposedException>(() => compressTask.Progress);

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void Dispose_And_Start_Should_ThrowException()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Dispose();
            var exception = Assert.Throws<ObjectDisposedException>(() => compressTask.Start());

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void Dispose_And_Wait_Should_ThrowException()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Dispose();
            var exception = Assert.Throws<ObjectDisposedException>(() => compressTask.Wait());

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void Dispose_And_Cancel_Should_ThrowException()
        {
            // Arrange
            using var sourceStream = new MemoryStream();
            using var writer = new BinaryWriter(sourceStream);
            writer.Write("Hello world");

            using var compressStream = new MemoryStream();
            using var compressTask = new GZipTaskFactory().Compress(sourceStream, compressStream);

            // Act
            compressTask.Dispose();
            var exception = Assert.Throws<ObjectDisposedException>(() => compressTask.Cancel());

            // Assert
            Assert.NotNull(exception);
        }
    }
}