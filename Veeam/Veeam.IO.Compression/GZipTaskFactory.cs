﻿using System;
using System.IO;
using Veeam.IO.Compression.Internal;

namespace Veeam.IO.Compression
{
    /// <summary>
    /// Provides support for creating GZipTask objects.
    /// </summary>
    public sealed class GZipTaskFactory
    {
        /// <summary>
        /// The default size of data block to compress.
        /// Every block compressing independently.
        /// </summary>
        private const Int32 DEFAULT_COMPRESSION_BLOCK_SIZE_IN_BYTES = 1024 * 1024; // 1MB

        /// <summary>
        /// Creates task to compress file by path.
        /// </summary>
        /// <param name="source">The source file to compress.</param>
        /// <param name="destination">the destination path to save compressed file.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Compress(String source, String destination)
        {
            return Compress(
                File.OpenRead(source),
                File.OpenWrite(destination),
                Environment.ProcessorCount,
                DEFAULT_COMPRESSION_BLOCK_SIZE_IN_BYTES);
        }

        /// <summary>
        /// Creates task to compress file by path.
        /// </summary>
        /// <param name="source">The source file to compress.</param>
        /// <param name="destination">the destination path to save compressed file.</param>
        /// <param name="threads">The count of threads to parallel compression.</param>
        /// <param name="blockSize">The data block size. Every block compressing independently.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Compress(String source, String destination, Int32 threads, Int32 blockSize)
        {
            return Compress(
                File.OpenRead(source),
                File.OpenWrite(destination),
                threads,
                blockSize);
        }

        /// <summary>
        /// Creates task to compress data from stream.
        /// </summary>
        /// <param name="source">The source stream to compress.</param>
        /// <param name="destination">the destination stream to save compressed data.</param>
        /// <param name="leaveOpenSourceStream">true to leave source stream open after the task object is disposed.</param>
        /// <param name="leaveOpenDestinationStream">true to leave destination stream open after the task object is disposed.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Compress(Stream source, Stream destination,
            Boolean leaveOpenSourceStream = false, Boolean leaveOpenDestinationStream = false)
        {
            return Compress(
                source,
                destination,
                Environment.ProcessorCount,
                DEFAULT_COMPRESSION_BLOCK_SIZE_IN_BYTES,
                leaveOpenSourceStream,
                leaveOpenDestinationStream);
        }

        /// <summary>
        /// Creates task to compress data from stream.
        /// </summary>
        /// <param name="source">The source stream to compress.</param>
        /// <param name="destination">the destination stream to save compressed data.</param>
        /// <param name="threads">The count of threads to parallel compression.</param>
        /// <param name="blockSize">The data block size. Every block compressing independently.</param>
        /// <param name="leaveOpenSourceStream">true to leave source stream open after the task object is disposed.</param>
        /// <param name="leaveOpenDestinationStream">true to leave destination stream open after the task object is disposed.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Compress(Stream source, Stream destination, Int32 threads, Int32 blockSize,
            Boolean leaveOpenSourceStream = false, Boolean leaveOpenDestinationStream = false)
        {
            var compressor = new Internal.Compression.DataBlockCompressor();
            var reader = new Internal.Compression.DataBlockStreamReader(source, blockSize, leaveOpenSourceStream);
            var writer = new Internal.Compression.DataBlockStreamWriter(destination, leaveOpenDestinationStream);
            return new GZipTask(reader, compressor, writer, threads);
        }

        /// <summary>
        /// Creates task to decompress file by path.
        /// </summary>
        /// <param name="source">The compressed file to decompress.</param>
        /// <param name="destination">the destination path to save decompressed file.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Decompress(String source, String destination)
        {
            return Decompress(
                File.OpenRead(source),
                File.OpenWrite(destination),
                Environment.ProcessorCount);
        }

        /// <summary>
        /// Creates task to decompress file by path.
        /// </summary>
        /// <param name="source">The compressed file to decompress.</param>
        /// <param name="destination">the destination path to save decompressed file.</param>
        /// <param name="threads">The count of threads to parallel decompression.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Decompress(String source, String destination, Int32 threads)
        {
            return Decompress(
                File.OpenRead(source),
                File.OpenWrite(destination),
                threads);
        }

        /// <summary>
        /// Creates task to decompress data from stream.
        /// </summary>
        /// <param name="source">The source stream to decompress data.</param>
        /// <param name="destination">the destination stream to save decompressed data.</param>
        /// <param name="leaveOpenSourceStream">true to leave source stream open after the task object is disposed.</param>
        /// <param name="leaveOpenDestinationStream">true to leave destination stream open after the task object is disposed.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Decompress(Stream source, Stream destination,
            Boolean leaveOpenSourceStream = false, Boolean leaveOpenDestinationStream = false)
        {
            return Decompress(
                source,
                destination,
                Environment.ProcessorCount,
                leaveOpenSourceStream,
                leaveOpenDestinationStream);
        }

        /// <summary>
        /// Creates task to decompress data from stream.
        /// </summary>
        /// <param name="source">The source stream to decompress data.</param>
        /// <param name="destination">the destination stream to save decompressed data.</param>
        /// <param name="threads">The count of threads to parallel decompression.</param>
        /// <param name="leaveOpenSourceStream">true to leave source stream open after the task object is disposed.</param>
        /// <param name="leaveOpenDestinationStream">true to leave destination stream open after the task object is disposed.</param>
        /// <returns>The task in idle state.</returns>
        public IGZipTask Decompress(Stream source, Stream destination, Int32 threads,
            Boolean leaveOpenSourceStream = false, Boolean leaveOpenDestinationStream = false)
        {
            var decompressor = new Internal.Decompression.DataBlockDecompressor();
            var reader = new Internal.Decompression.DataBlockStreamReader(source, leaveOpenSourceStream);
            var writer = new Internal.Decompression.DataBlockStreamWriter(destination, leaveOpenDestinationStream);
            return new GZipTask(reader, decompressor, writer, threads);
        }
    }
}