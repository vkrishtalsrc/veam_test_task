﻿using System;

namespace Veeam.IO.Compression
{
    /// <summary>
    /// Represents a result of <see cref="IGZipTask"/> execution.
    /// </summary>
    public sealed class GZipTaskResult
    {
        /// <summary>
        /// Creates result which signals about successfully completed task..
        /// </summary>
        internal static GZipTaskResult Ok() => new GZipTaskResult(true, null);

        /// <summary>
        /// Creates result which signals about finished with errors task.
        /// </summary>
        internal static GZipTaskResult Fail(Exception exception) => new GZipTaskResult(false, exception);

        /// <summary>
        /// true if the task completed without error; otherwise false.
        /// </summary>
        public Boolean Success { get; }

        /// <summary>
        /// The exception.
        /// Must be null if no any exceptions.
        /// </summary>
        public Exception Exception { get; }

        /// <summary>
        /// Creates a new instance of <see cref="GZipTaskResult"/> class.
        /// </summary>
        private GZipTaskResult(Boolean success, Exception exception)
        {
            Success = success;
            Exception = exception;
        }
    }
}