﻿using System;

namespace Veeam.IO.Compression
{
    /// <summary>
    /// Provides facilities to control parallel zip task.
    /// </summary>
    public interface IGZipTask : IDisposable
    {
        /// <summary>
        /// True if tasks in progress; otherwise false.
        /// </summary>
        Boolean IsRunning { get; }

        /// <summary>
        /// The task progress.
        /// </summary>
        Int64 Progress { get; }

        /// <summary>
        /// The task result.
        /// </summary>
        GZipTaskResult Result { get; }

        /// <summary>
        /// Starts the task execution in the thread.
        /// </summary>
        void Start();

        /// <summary>
        /// Blocks current thread and waits until task finished.
        /// </summary>
        void Wait();

        /// <summary>
        /// Cancels the task execution.
        /// </summary>
        void Cancel();
    }
}