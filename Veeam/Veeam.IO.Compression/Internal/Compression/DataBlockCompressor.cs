﻿using System;
using System.IO;
using System.IO.Compression;

namespace Veeam.IO.Compression.Internal.Compression
{
    /// <summary>
    /// Provides facilities to compress <see cref="DataBlock"/> objects.
    /// </summary>
    internal sealed class DataBlockCompressor : IDataBlockProcessor
    {
        /// <summary>
        /// Compresses <see cref="DataBlock"/> object.
        /// </summary>
        /// <param name="block">The object to compress.</param>
        /// <returns>Compressed data block.</returns>
        public DataBlock Process(DataBlock block)
        {
            if (block == null)
            {
                throw new ArgumentNullException(nameof(block));
            }

            using var output = new MemoryStream();
            using var zip = new GZipStream(output, CompressionLevel.NoCompression);

            zip.Write(block.Data, 0, block.Data.Length);
            zip.Flush();

            return new DataBlock(output.ToArray(), block.Index);
        }
    }
}