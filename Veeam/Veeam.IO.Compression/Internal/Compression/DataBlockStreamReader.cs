﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Veeam.IO.Compression.Internal.Compression
{
    // Why we need it?
    // We could use Stream.Synchronized(Stream) method to gain thread-safety stream and avoid custom implementation.
    // However the test task contains specific requirements to avoid existing high level mechanisms of thread synchronization.
    // More information about Stream.Synchronized(Stream): https://docs.microsoft.com/en-us/dotnet/api/system.io.stream.synchronized?view=netcore-3.1

    /// <summary>
    /// Represents a simple thread-safety stream reader of DataBlock objects.
    /// This reader must be used to read not compressed data blocks.
    /// </summary>
    internal sealed class DataBlockStreamReader : IDataBlockReader
    {
        /// <summary>
        /// The lock guard.
        /// </summary>
        private readonly Object m_lock = new Object();

        /// <summary>
        /// The user specified block size to read.
        /// </summary>
        private readonly Int32 m_blockSize;

        /// <summary>
        /// The internal binary reader.
        /// </summary>
        private readonly BinaryReader m_reader;

        /// <summary>
        /// The current block index.
        /// </summary>
        private Int64 m_currentIndex;

        /// <summary>
        /// true if objects have already disposed; otherwise false.
        /// </summary>
        private volatile int m_disposed;

        /// <summary>
        /// Creates a new instance of <see cref="DataBlockStreamReader"/>.
        /// </summary>
        /// <param name="stream">The stream to read data.</param>
        /// <param name="blockSize">The block size.</param>
        /// <param name="leaveOpen">true to leave source stream open after the object is disposed.</param>
        public DataBlockStreamReader(Stream stream, Int32 blockSize, Boolean leaveOpen = false)
        {
            if (blockSize < 1)
            {
                throw new ArgumentException($"Invalid block size: {blockSize} bytes.", nameof(blockSize));
            }

            m_blockSize = blockSize;
            m_reader = new BinaryReader(stream, Encoding.Default, leaveOpen);
        }

        /// <summary>
        /// Disposes the instance.
        /// </summary>
        public void Dispose()
        {
            // The current class is sealed - virtual void Dispose(bool disposing) method is not needed.
            // The class does not contain any unmanagement resources - Finalization is not needed.
            // More information: https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose?redirectedfrom=MSDN

            // thread-safety dispose
            if (Interlocked.CompareExchange(ref m_disposed, 1, 0) == 0)
            {
                m_reader?.Dispose();
            }
        }

        /// <summary>
        /// Tries to read next data block of specified block size.
        /// </summary>
        /// <param name="dataBlock">The result data block.</param>
        /// <returns>true if block was read; otherwise false.</returns>
        public Boolean Read(out DataBlock dataBlock)
        {
            lock (m_lock)
            {
                ThrowIfDisposed();

                if (m_reader.BaseStream.Position == m_reader.BaseStream.Length)
                {
                    dataBlock = null;
                    return false;
                }

                var buffer = m_reader.ReadBytes(m_blockSize);
                if (buffer.Length == 0)
                {
                    dataBlock = null;
                    return false;
                }

                // create result block and increase block index
                dataBlock = new DataBlock(buffer, ++m_currentIndex);
                return true;
            }
        }

        /// <summary>
        /// Resets stream position to begin.
        /// </summary>
        public void Reset()
        {
            lock (m_lock)
            {
                ThrowIfDisposed();
                m_currentIndex = 0;
                m_reader.BaseStream.Seek(0, SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Throws exception if object have already disposed.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ThrowIfDisposed()
        {
            if (m_disposed == 1)
            {
                throw new ObjectDisposedException(nameof(DataBlockStreamReader));
            }
        }
    }
}