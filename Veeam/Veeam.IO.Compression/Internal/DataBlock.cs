﻿using System;

namespace Veeam.IO.Compression.Internal
{
    /// <summary>
    /// Represents a data block to compress or decompress.
    /// </summary>
    internal sealed class DataBlock
    {
        /// <summary>
        /// The block index (order).
        /// </summary>
        public Int64 Index { get; }

        /// <summary>
        /// The block binary data.
        /// </summary>
        public Byte[] Data { get; }

        /// <summary>
        /// Creates a new instance of <see cref="DataBlock"/> class.
        /// </summary>
        /// <param name="data">The binary data.</param>
        /// <param name="index">The block index.</param>
        public DataBlock(Byte[] data, Int64 index)
        {
            Index = index;
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }
    }
}