﻿using System;
using System.IO;
using System.IO.Compression;

namespace Veeam.IO.Compression.Internal.Decompression
{
    /// <summary>
    /// Provides facilities to decompress <see cref="DataBlock"/> objects.
    /// </summary>
    internal sealed class DataBlockDecompressor : IDataBlockProcessor
    {
        /// <summary>
        /// Decompresses the data block.
        /// </summary>
        public DataBlock Process(DataBlock block)
        {
            if (block == null)
            {
                throw new ArgumentNullException(nameof(block));
            }

            using var input = new MemoryStream(block.Data);
            using var output = new MemoryStream();
            using var zip = new GZipStream(input, CompressionMode.Decompress);
            zip.CopyTo(output);
            zip.Flush();
            return new DataBlock(output.ToArray(), block.Index);
        }
    }
}