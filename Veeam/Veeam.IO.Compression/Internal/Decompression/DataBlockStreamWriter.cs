﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Veeam.IO.Compression.Internal.Decompression
{
    // Why we need it?
    // We could use Stream.Synchronized(Stream) method to gain thread-safety stream and avoid custom implementation.
    // However the test task contains specific requirements to avoid existing high level mechanisms of thread synchronization.
    // More information: https://docs.microsoft.com/en-us/dotnet/api/system.io.stream.synchronized?view=netcore-3.1

    /// <summary>
    /// Represents writer to write decompressed data blocks.
    /// </summary>
    internal sealed class DataBlockStreamWriter : IDataBlockWriter
    {
        /// <summary>
        /// The lock guard.
        /// </summary>
        private readonly Object m_lock = new Object();

        /// <summary>
        /// The internal binary writer.
        /// </summary>
        private readonly BinaryWriter m_writer;

        /// <summary>
        /// true if objects have already disposed; otherwise false.
        /// </summary>
        private volatile Int32 m_disposed;

        /// <summary>
        /// Creates a new instance of <see cref="DataBlockStreamWriter"/> class.
        /// </summary>
        public DataBlockStreamWriter(Stream stream, Boolean leaveOpen = false)
        {
            m_writer = new BinaryWriter(stream, Encoding.Default, leaveOpen);
        }

        /// <summary>
        /// Disposed the instance.
        /// </summary>
        public void Dispose()
        {
            // The current class is sealed - virtual void Dispose(bool disposing) method is not needed.
            // The class does not contain any unmanagement resources - Finalization is not required.
            // More information: https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose?redirectedfrom=MSDN

            // thread-safety dispose
            if (Interlocked.CompareExchange(ref m_disposed, 1, 0) == 0)
            {
                m_writer?.Dispose();
            }
        }

        /// <summary>
        /// Writers the not compressed data block.
        /// </summary>
        /// <param name="dataBlock"></param>
        public void Write(DataBlock dataBlock)
        {
            lock (m_lock)
            {
                ThrowIfDisposed();
                m_writer.Write(dataBlock.Data);
            }
        }

        /// <summary>
        /// Sets stream position to begin.
        /// </summary>
        public void Reset()
        {
            lock (m_lock)
            {
                ThrowIfDisposed();
                m_writer.Seek(0, SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Throws exception if instance have already disposed.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ThrowIfDisposed()
        {
            if (m_disposed == 1)
            {
                throw new ObjectDisposedException(nameof(DataBlockStreamWriter));
            }
        }
    }
}