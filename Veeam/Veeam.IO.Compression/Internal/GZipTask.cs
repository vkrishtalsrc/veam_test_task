﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Stateless;
using Veeam.Collections;

namespace Veeam.IO.Compression.Internal
{
    /// <summary>
    /// Represents an multi-threading operation to compress or decompress data.
    /// </summary>
    internal sealed class GZipTask : IGZipTask
    {
        /// <summary>
        /// The max count of item in thread-safety queues.
        /// </summary>
        private const Int32 MAX_QUEUE_SIZE = 32;

        /// <summary>
        /// The state of task object.
        /// </summary>
        private enum State
        {
            /// <summary>
            /// The task was not started.
            /// </summary>
            Idle,

            /// <summary>
            /// The task started.
            /// </summary>
            InProgress,

            /// <summary>
            /// The task completed successful.
            /// </summary>
            Completed,

            /// <summary>
            /// The task finished with errors.
            /// </summary>
            Failed,

            /// <summary>
            /// The tasks was canceled by user.
            /// </summary>
            Canceled,

            /// <summary>
            /// The tasks object was disposed.
            /// </summary>
            Disposed
        }

        /// <summary>
        /// The triggers to change state of task object.
        /// </summary>
        private enum Trigger
        {
            /// <summary>
            /// The trigger to start task.
            /// </summary>
            Start,

            /// <summary>
            /// The trigger to complete task.
            /// </summary>
            Success,

            /// <summary>
            /// The signal task competed with objects.
            /// </summary>
            Fail,

            /// <summary>
            /// The trigger to cancel task.
            /// </summary>
            Cancel,

            /// <summary>
            /// The trigger to dispose task.
            /// </summary>
            Dispose
        }

        /// <summary>
        /// The state machine.
        /// Note! The <see cref="StateMachine{TState,TTrigger}"/> class is not thread-safety!
        /// Use Fire(Trigger) and GetCurrentState() thread-safety methods only to work on state machine!
        /// </summary>
        private readonly StateMachine<State, Trigger> m_stateMachine =
            new StateMachine<State, Trigger>(State.Idle);

        /// <summary>
        /// The parameterized trigger to signal about errors.
        /// </summary>
        private readonly StateMachine<State, Trigger>.TriggerWithParameters<Exception> m_failedTrigger =
            new StateMachine<State, Trigger>.TriggerWithParameters<Exception>(Trigger.Fail);

        /// <summary>
        /// The lock that is used to manage access to state machine and state of task object,
        /// allowing multiple threads for reading or exclusive access for writing.
        /// It is useful because we have to often read the state but rare change it.
        /// </summary>
        private readonly ReaderWriterLockSlim m_readerWriterLockSlim = new ReaderWriterLockSlim();

        /// <summary>
        /// The provider to get data to compress or decompress.
        /// </summary>
        private readonly IDataBlockReader m_dataBlockReader;

        /// <summary>
        /// The data handler to work on data blocks. It can be data compress or decompressor.
        /// </summary>
        private readonly IDataBlockProcessor m_dataBlockProcessor;

        /// <summary>
        /// The consumer of handled data blocks. It can be file stream, database and so on.
        /// </summary>
        private readonly IDataBlockWriter m_dataBlockWriter;

        /// <summary>
        /// The specified by user thread count to handle data blocks.
        /// </summary>
        private readonly Int32 m_threadsCount;

        /// <summary>
        /// The thread-safety queue to store input data.
        /// </summary>
        private SynchronizedQueue<DataBlock> m_inputQueue;

        /// <summary>
        /// The thread-safety priority-queue to store compressed\decompressed data.
        /// The queue saves real order of data blocks.
        /// </summary>
        private SynchronizedPriorityQueue<Int64, DataBlock> m_outputQueue;

        /// <summary>
        /// The count of handled bytes.
        /// </summary>
        private Int64 m_progressInBytes;

        /// <summary>
        /// The task result.
        /// It is null until task not completed.
        /// </summary>
        private volatile GZipTaskResult m_result;

        /// <summary>
        /// The thread manager to start\wait threads.
        /// </summary>
        private ThreadManager m_threadManager;

        /// <summary>
        /// The task execution progress in bytes.
        /// The value show count of handled bytes.
        /// </summary>
        public Int64 Progress
        {
            get
            {
                ThrowIfDisposed();
                return Interlocked.Read(ref m_progressInBytes);
            }
        }

        /// <summary>
        /// The task result.
        /// It is null until due the task in progress.
        /// </summary>
        public GZipTaskResult Result
        {
            get
            {
                ThrowIfDisposed();
                return m_result;
            }
        }

        /// <summary>
        /// True if task in progress; otherwise false.
        /// </summary>
        public Boolean IsRunning
        {
            get
            {
                var state = GetCurrentState();
                ThrowIfDisposed(state);
                return state == State.InProgress;
            }
        }

        /// <summary>
        /// Creates a new instance of <see cref="GZipTask"/> class.
        /// </summary>
        /// <param name="dataBlockReader">The provider of source data.</param>
        /// <param name="dataBlockProcessor">The handler of data. Compressor or decompressor.</param>
        /// <param name="dataBlockWriter">The consumer of handled data (File, database, network etc).</param>
        /// <param name="threads">The count of thread to handle data.</param>
        public GZipTask(IDataBlockReader dataBlockReader, IDataBlockProcessor dataBlockProcessor,
            IDataBlockWriter dataBlockWriter, Int32 threads)
        {
            m_threadsCount = threads > 1 ? threads : 1; // minimum one thread will be used
            m_dataBlockReader = dataBlockReader ?? throw new ArgumentNullException(nameof(dataBlockReader));
            m_dataBlockProcessor = dataBlockProcessor ?? throw new ArgumentNullException(nameof(dataBlockProcessor));
            m_dataBlockWriter = dataBlockWriter ?? throw new ArgumentNullException(nameof(dataBlockWriter));

            ConfigureStateMachine();
        }

        /// <summary>
        /// Starts the task.
        /// The method does not block current thread.
        /// </summary>
        public void Start()
        {
            ThrowIfDisposed();
            Fire(Trigger.Start);
        }

        /// <summary>
        /// Blocks current thread to wait task finished.
        /// </summary>
        public void Wait()
        {
            // check current state
            var state = GetCurrentState();
            ThrowIfDisposed(state);

            // and try start if it is needed
            if (state != State.InProgress)
            {
                Fire(Trigger.Start);
            }

            // wait all threads
            m_threadManager.WaitAll();

            // throw exception if error was raised
            if (m_result?.Exception != null)
            {
                throw m_result.Exception;
            }
        }

        /// <summary>
        ///  Cancels the task.
        /// </summary>
        public void Cancel()
        {
            ThrowIfDisposed();
            Fire(Trigger.Cancel);
        }

        /// <summary>
        /// Disposes the task object.
        /// </summary>
        public void Dispose()
        {
            Fire(Trigger.Dispose);
        }

        /// <summary>
        /// Handles <see cref="Trigger.Start"/> trigger.
        /// </summary>
        private void OnStarted()
        {
            // notify providers we would start from begin
            m_dataBlockReader.Reset();
            m_dataBlockWriter.Reset();

            // reset task state
            ResetProgress();
            CreateThreadSafetyQueues();

            // start task execution
            BeginExecution();
        }

        /// <summary>
        /// Handles <see cref="Trigger.Success"/> trigger.
        /// </summary>
        private void OnCompleted()
        {
            // task completed without errors
            m_result = GZipTaskResult.Ok();
        }

        /// <summary>
        /// Handles <see cref="Trigger.Fail"/> trigger.
        /// </summary>
        /// <param name="exception"></param>
        private void OnFailed(Exception exception)
        {
            // the task finished with error
            m_result = GZipTaskResult.Fail(exception);
        }

        /// <summary>
        /// Handles <see cref="Trigger.Cancel"/> trigger.
        /// </summary>
        private void OnCanceled()
        {
            // the task was canceled by used
            m_result = GZipTaskResult.Fail(new OperationCanceledException(nameof(GZipTask)));
        }

        /// <summary>
        /// Handles <see cref="Trigger.Dispose"/> trigger.
        /// </summary>
        private void OnDisposed()
        {
            // must be called from state machine at once only.
            m_dataBlockReader?.Dispose();
            m_dataBlockWriter?.Dispose();
        }

        /// <summary>
        /// Reads input data blocks.
        /// </summary>
        private void ConsumeDataBlocks()
        {
            try
            {
                // get source data
                while (IsRunning && m_dataBlockReader.Read(out var block))
                {
                    // put data to queue on processing (compressing or decompressing)
                    m_inputQueue.Enqueue(block);
                }
            }
            catch (Exception exception)
            {
                // notify state machine about exception
                Fire(m_failedTrigger, exception);
            }
            finally
            {
                // release the queue
                // it means current thread will not enqueue any data
                m_inputQueue.Release();
            }
        }

        /// <summary>
        /// Handles input data blocks.
        /// </summary>
        private void HandleDataBlocks()
        {
            try
            {
                // get source data from queue to processing.
                while (IsRunning && (m_inputQueue.NewItemsExpected || !m_inputQueue.IsEmpty()))
                {
                    // Try to dequeue block.
                    // If the queue is empty the current thread are blocks and waits new data added.
                    if (m_inputQueue.Dequeue(out var block))
                    {
                        // data block was got
                        // handle this block
                        var compressedBlock = m_dataBlockProcessor.Process(block);

                        // add processed block to result queue
                        m_outputQueue.Enqueue(compressedBlock, compressedBlock.Index);

                        // update progress (progress is a sum of processed source bytes)
                        Interlocked.Add(ref m_progressInBytes, block.Data.Length);
                    }
                }
            }
            catch (Exception exception)
            {
                // notify state machine about exception
                Fire(m_failedTrigger, exception);
            }
            finally
            {
                // release the queue
                // it means current thread will not enqueue any data
                m_outputQueue.Release();
            }
        }

        /// <summary>
        /// Provides result blocks.
        /// </summary>
        private void ProvideDataBlocks()
        {
            try
            {
                // get processed data blocks from result queue
                while (m_outputQueue.NewItemsExpected || !m_outputQueue.IsEmpty())
                {
                    // Try to dequeue block.
                    // If the queue is empty the current thread are blocks and waits new data added.
                    if (m_outputQueue.Dequeue(out var block))
                    {
                        // send block to consumer (file \ database \ network etc)
                        m_dataBlockWriter.Write(block);
                    }
                }
            }
            catch (Exception exception)
            {
                // notify state machine about exception
                Fire(m_failedTrigger, exception);
            }
        }

        /// <summary>
        /// Resets current progress.
        /// </summary>
        private void ResetProgress()
        {
            m_progressInBytes = 0;
            m_result = null;
        }

        /// <summary>
        /// Creates thread-safety queues.
        /// </summary>
        private void CreateThreadSafetyQueues()
        {
            // the queue to store input data
            m_inputQueue = new SynchronizedQueue<DataBlock>(MAX_QUEUE_SIZE);

            // the queue to store processed result data
            m_outputQueue = new SynchronizedPriorityQueue<Int64, DataBlock>(MAX_QUEUE_SIZE, producers: m_threadsCount);
        }

        /// <summary>
        /// Starts task execution.
        /// </summary>
        private void BeginExecution()
        {
            // I decided to use by one thread to consume and provide data because it is more optimal way in my view.
            // However it is easy to increase thread count here because logic inside threads is thread-safety.
            var threadActions = new List<Action>
            {
                ConsumeDataBlocks, // reads input data
                ProvideDataBlocks // saves handled data
            };

            // create many threads to handle data
            threadActions.AddRange(Enumerable.Repeat<Action>(HandleDataBlocks, m_threadsCount));

            // create object to manage created threads
            m_threadManager = new ThreadManager(threadActions, () =>
            {
                // the callback which will used when threads finished
                if (IsRunning)
                {
                    Fire(Trigger.Success);
                }
            });

            m_threadManager.StartAll();
        }

        /// <summary>
        /// Configures internal task state machine.
        /// </summary>
        private void ConfigureStateMachine()
        {
            m_stateMachine.Configure(State.Idle)
                .Permit(Trigger.Start, State.InProgress)
                .Permit(Trigger.Dispose, State.Disposed);

            m_stateMachine.Configure(State.InProgress)
                .OnEntry(OnStarted)
                .Permit(Trigger.Success, State.Completed)
                .Permit(Trigger.Fail, State.Failed)
                .Permit(Trigger.Cancel, State.Canceled)
                .Permit(Trigger.Dispose, State.Disposed)
                .Ignore(Trigger.Start);

            m_stateMachine.Configure(State.Completed)
                .OnEntry(OnCompleted)
                .Permit(Trigger.Start, State.InProgress)
                .Permit(Trigger.Dispose, State.Disposed)
                .Ignore(Trigger.Success);

            m_stateMachine.Configure(State.Failed)
                .OnEntryFrom(m_failedTrigger, OnFailed)
                .Permit(Trigger.Start, State.InProgress)
                .Permit(Trigger.Dispose, State.Disposed)
                .Ignore(Trigger.Fail)
                .Ignore(Trigger.Cancel)
                .Ignore(Trigger.Success);

            m_stateMachine.Configure(State.Canceled)
                .OnEntry(OnCanceled)
                .Permit(Trigger.Start, State.InProgress)
                .Permit(Trigger.Dispose, State.Disposed)
                .Ignore(Trigger.Cancel)
                .Ignore(Trigger.Fail)
                .Ignore(Trigger.Success);

            m_stateMachine.Configure(State.Disposed)
                .OnEntry(OnDisposed)
                .Ignore(Trigger.Dispose);
        }

        /// <summary>
        /// The thread-safety way to change task state.
        /// </summary>
        private void Fire(Trigger trigger)
        {
            try
            {
                m_readerWriterLockSlim.EnterWriteLock();
                m_stateMachine.Fire(trigger);
            }
            finally
            {
                m_readerWriterLockSlim.ExitWriteLock();
            }
        }

        /// <summary>
        /// The thread-safety way to change task state.
        /// </summary>
        private void Fire<T>(StateMachine<State, Trigger>.TriggerWithParameters<T> trigger, T param)
        {
            try
            {
                m_readerWriterLockSlim.EnterWriteLock();
                m_stateMachine.Fire(trigger, param);
            }
            finally
            {
                m_readerWriterLockSlim.ExitWriteLock();
            }
        }

        /// <summary>
        /// The thread-safety way to get task state.
        /// </summary>
        private State GetCurrentState()
        {
            try
            {
                m_readerWriterLockSlim.EnterReadLock();
                var state = m_stateMachine.State;
                return state;
            }
            finally
            {
                m_readerWriterLockSlim.ExitReadLock();
            }
        }

        /// <summary>
        /// Throw exception if task have already disposed.
        /// </summary>
        private void ThrowIfDisposed()
        {
            var state = GetCurrentState();
            ThrowIfDisposed(state);
        }

        /// <summary>
        /// Throw exception if task have already disposed.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ThrowIfDisposed(State state)
        {
            if (state == State.Disposed)
            {
                throw new ObjectDisposedException(nameof(GZipTask));
            }
        }
    }
}