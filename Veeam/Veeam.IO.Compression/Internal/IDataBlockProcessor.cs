﻿namespace Veeam.IO.Compression.Internal
{
    /// <summary>
    /// Provides method to process data blocks.
    /// </summary>
    internal interface IDataBlockProcessor
    {
        /// <summary>
        /// Returns processed data block.
        /// </summary>
        /// <param name="block">Data block to process.</param>
        /// <returns>Process data block.</returns>
        DataBlock Process(DataBlock block);
    }
}