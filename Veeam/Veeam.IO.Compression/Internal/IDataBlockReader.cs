﻿using System;

namespace Veeam.IO.Compression.Internal
{
    /// <summary>
    /// Provides facilities to read data blocks.
    /// </summary>
    internal interface IDataBlockReader : IDisposable
    {
        /// <summary>
        /// Tries to read data block.
        /// </summary>
        /// <param name="dataBlock">The output data.</param>
        /// <returns>true if data was read; otherwise false.</returns>
        Boolean Read(out DataBlock dataBlock);

        /// <summary>
        /// Sets reader position to begin.
        /// </summary>
        void Reset();
    }
}