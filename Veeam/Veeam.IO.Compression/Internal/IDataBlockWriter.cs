﻿using System;

namespace Veeam.IO.Compression.Internal
{
    /// <summary>
    /// Provides facilities to write data blocks.
    /// </summary>
    internal interface IDataBlockWriter : IDisposable
    {
        /// <summary>
        /// Writes data block.
        /// </summary>
        /// <param name="dataBlock">The data to write.</param>
        void Write(DataBlock dataBlock);

        /// <summary>
        /// Sets writer position to begin.
        /// </summary>
        void Reset();
    }
}