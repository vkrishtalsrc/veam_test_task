﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Veeam.IO.Compression.Internal
{
    /// <summary>
    /// Represents a simple tread manager to start set of threads.
    /// </summary>
    internal sealed class ThreadManager
    {
        /// <summary>
        /// The callback which will executed when all threads finished.
        /// </summary>
        private readonly Action m_onFinishedCallback;

        /// <summary>
        /// The threads to manage.
        /// </summary>
        private readonly Thread[] m_threads;

        /// <summary>
        /// </summary>
        private volatile Int32 m_activeThreads;

        /// <summary>
        /// Creates a new instance of <see cref="ThreadManager"/> class.
        /// </summary>
        /// <param name="actions">the thread actions.</param>
        /// <param name="onFinishedCallback">The callback to use after all threads completed.</param>
        public ThreadManager(IEnumerable<Action> actions, Action onFinishedCallback = null)
        {
            if (actions == null)
            {
                throw new ArgumentNullException(nameof(actions));
            }

            m_onFinishedCallback = onFinishedCallback;
            m_threads = actions.Select(CreateThread).ToArray();
        }

        /// <summary>
        /// Starts all threads.
        /// </summary>
        public void StartAll()
        {
            if (m_activeThreads > 0)
            {
                throw new InvalidOperationException("Threads have already started.");
            }

            foreach (var thread in m_threads)
            {
                thread.Start();
            }
        }

        /// <summary>
        /// Wait all threads finished.
        /// </summary>
        public void WaitAll()
        {
            if (m_activeThreads < 1)
            {
                StartAll();
            }

            foreach (var thread in m_threads)
            {
                thread.Join();
            }
        }

        /// <summary>
        /// Creates thread for action.
        /// </summary>
        private Thread CreateThread(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            return new Thread(() =>
            {
                // increase counter of active threads
                Interlocked.Increment(ref m_activeThreads);

                try
                {
                    action();
                }
                finally
                {
                    // decrease counter of active threads
                    if (Interlocked.Decrement(ref m_activeThreads) == 0)
                    {
                        // invoke callback because it was last thread
                        m_onFinishedCallback?.Invoke();
                    }
                }
            });
        }
    }
}